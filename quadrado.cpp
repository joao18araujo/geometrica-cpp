#include "quadrado.hpp"

Quadrado::Quadrado(){
    setBase(5);
    setAltura(5);
}

Quadrado::Quadrado(float base){
    setBase(base);
    setAltura(base);
}

float Quadrado::area(){
    return getBase() * getBase();
}

float Quadrado::area(float base){
	return base * base;
}
