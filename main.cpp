#include "retangulo.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"

using namespace std;

int main(){

	Geometrica * formaRetangulo = new Retangulo(12, 8);
	Geometrica * formaQuadrado = new Quadrado(9);
	Geometrica * formaTriangulo = new Triangulo(11, 10);

	cout << "Calculo da Area do Retangulo: " << formaRetangulo->area() << endl; 
	cout << "Calculo da Area do Triangulo: " << formaQuadrado->area() << endl; 
	cout << "Calculo da Area do Quadrado: " << formaTriangulo->area() << endl; 

    return 0;
}
