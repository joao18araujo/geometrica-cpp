#include "geometrica.hpp"

Geometrica::Geometrica(){
	setBase(10);
	setAltura(10);
}

Geometrica::Geometrica(float base, float altura){
	setBase(base);
	setAltura(altura);
}

float Geometrica::getBase(){
	return base;
}

float Geometrica::getAltura(){
	return altura;
}

void Geometrica::setBase(float base){
	this->base = base;
}

void Geometrica::setAltura(float altura){
	this->altura = altura;
}
