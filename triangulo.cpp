#include "triangulo.hpp"

Triangulo::Triangulo(){
    setBase(3);
    setAltura(4);
}

Triangulo::Triangulo(float base, float altura){
    setBase(base);
    setAltura(altura);
}

float Triangulo::area(){
    return (getBase() * getAltura())/2.0;
}

float Triangulo::area(float base, float altura){
	return (base * altura)/2.0;
}
