#ifndef QUADRADO_H
#define QUADRADO_H

#include "geometrica.hpp"

class Quadrado : public Geometrica{
    	public:
        	Quadrado();
        	Quadrado(float base);
        	float area();
		float area (float base);
};

#endif
