all:		geometrica.o retangulo.o triangulo.o quadrado.o main.o
		g++ -o main geometrica.o retangulo.o triangulo.o quadrado.o main.o

geometrica.o:	geometrica.cpp geometrica.hpp
		g++ -c geometrica.cpp

retangulo.o:	retangulo.cpp retangulo.hpp
		g++ -c retangulo.cpp
		
quadrado.o:		quadrado.cpp quadrado.hpp
		g++ -c quadrado.cpp


triangulo.o:	triangulo.cpp triangulo.hpp
		g++ -c triangulo.cpp

main.o:		main.cpp retangulo.hpp
		g++ -c main.cpp

clean:
		rm -rf *.o

run:
		./main
