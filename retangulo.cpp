#include "retangulo.hpp"

Retangulo::Retangulo(){
    setBase(10);
    setAltura(15);
}

Retangulo::Retangulo(float base, float altura){
    setBase(base);
    setAltura(altura);
}

float Retangulo::area(){
    return getBase() * getAltura();
}

float Retangulo::area(float base, float altura){
	return base * altura;
}


