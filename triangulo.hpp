#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "geometrica.hpp"

class Triangulo : public Geometrica{
    	public:
        	Triangulo();
        	Triangulo(float base, float altura);
        	float area();
		float area (float base, float altura);
};

#endif
