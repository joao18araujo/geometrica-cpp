#ifndef RETANGULO_H
#define RETANGULO_H

#include "geometrica.hpp"

class Retangulo : public Geometrica{
    	public:
        	Retangulo();
        	Retangulo(float base, float altura);
        	float area();
		float area (float base, float altura);
};

#endif
