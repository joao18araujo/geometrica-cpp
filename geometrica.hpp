#ifndef GEOMETRICA_H
#define GEOMETRICA_H

#include <iostream>

class Geometrica{
    	private:
        	float base, altura;
        
    	public:
        	Geometrica();
        	Geometrica(float base, float altura);
		float getBase();
        	float getAltura();
        	void setBase(float base);
       	void setAltura(float altura);
		virtual float area() = 0;
};

#endif
